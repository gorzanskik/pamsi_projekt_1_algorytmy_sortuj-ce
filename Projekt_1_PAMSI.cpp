﻿#include <iostream>
#include <time.h>
#include <cstdlib>
#include <string>
#include <fstream>

using namespace std;

time_t start_q, stop_q, start_m, stop_m, start_h, stop_h, start_s, stop_s;
double czas_q, czas_m, czas_h, czas_s;

void Quicksort(int *tablica, int lewy, int prawy) {

    int pivot = (lewy + prawy) / 2;
    swap(tablica[pivot], tablica[prawy]);
    int i,j;
    i = lewy;
    j = lewy;
    
    for (int f = lewy; f < prawy; f++) {
            if (tablica[i] < tablica[prawy]) {
                swap(tablica[i], tablica[j]);
                i++;
                j++;
            }
            else
            i++;
    }

    swap(tablica[j], tablica[prawy]);
        
    if (lewy < j - 1)  
        Quicksort(tablica, lewy, j - 1);
    if (j + 1 < prawy) 
        Quicksort(tablica, j + 1, prawy);

}

void Mergesort(int* tablica, int *tablica_p, int i_p, int i_k) {

    int i_s = (i_p + i_k + 1) / 2;
    int bufor_p, bufor_s;

    if (i_s - 1 - i_p > 0) 
        Mergesort(tablica, tablica_p, i_p, i_s - 1);
    if (i_k - i_s > 0) 
        Mergesort(tablica, tablica_p, i_s, i_k);

    bufor_p = i_p;
    bufor_s = i_s;

    for (int i = i_p; i <= i_k; i++) {

        tablica_p[i] = ((bufor_p == i_s) || ((bufor_s <= i_k) && (tablica[bufor_p] > tablica[bufor_s]))) ? tablica[bufor_s++] : tablica[bufor_p++];
        
    }

    int pomoc = i_k;

    for (int i = i_p; i <= i_k; i++) {

        tablica[i] = tablica_p[i];
    }

}

void Heapsort(int *tablica, int ile) {

    int i, j, k, x;

    for (int i = 2; i <= ile; i++){

        j = i; k = j / 2;
        x = tablica[i];
        while ((k > 0) && (tablica[k] < x))
        {
            tablica[j] = tablica[k];
            j = k; k = j / 2;
        }
        tablica[j] = x;
    }

    int m;

    for (i = ile; i > 1; i--){

        swap(tablica[1], tablica[i]);
        j = 1; 
        k = 2;

        while (k < i)
        {
            if ((k + 1 < i) && (tablica[k + 1] > tablica[k]))
                m = k + 1;
            else
                m = k;
            if (tablica[m] <= tablica[j]) 
                break;
            swap(tablica[j], tablica[m]);
            j = m; 
            k = j + j;
        }
    }

}

int main() {

    fstream plik_q, plik_m, plik_h;
    
    srand((unsigned)time(NULL));
    int ile;
    cout  << "Podaj rozmiar tablicy: ";
    cin >> ile;
    
    for (int f = 0; f < 2; f++) {

        int* tablica_pomiar, * wskaznik_pomiar, * tablica_pomiar_p, * wskaznik_pomiar_p;  //MERGESORT DO POMIAROW
        tablica_pomiar = new int[ile];
        tablica_pomiar_p = new int[ile];
        wskaznik_pomiar = tablica_pomiar;
        wskaznik_pomiar_p = tablica_pomiar_p;

        for (int i = 0; i < ile; i++) {
            *wskaznik_pomiar = rand() % 100 + 1;   //LICZBY PSEUDOLOSOWE
            wskaznik_pomiar++;
        }

        wskaznik_pomiar = tablica_pomiar;
        wskaznik_pomiar_p = tablica_pomiar_p;
        Mergesort(wskaznik_pomiar, wskaznik_pomiar_p, 0, ile * 0.997 - 1);
        wskaznik_pomiar = tablica_pomiar;
        wskaznik_pomiar_p = tablica_pomiar_p;

        int* tablica_q, * wskaznik_q;  //QUICKSORT
        tablica_q = new int[ile];
        wskaznik_q = tablica_q;

        int* tablica_m, * wskaznik_m, * tablica_m_p, * wskaznik_m_p;  //MERGESORT
        tablica_m = new int[ile];
        tablica_m_p = new int[ile];
        wskaznik_m = tablica_m;
        wskaznik_m_p = tablica_m_p;

        int* tablica_h, * wskaznik_h;    //HEAPSORT
        tablica_h = new int [ile + 1];
        wskaznik_h = tablica_h;
        wskaznik_h++;

        for (int i = 0; i < ile; i++) {     
            *wskaznik_q = rand()%100+1;  //LICZBY PSEUDOLOSOWE
            *wskaznik_m = *wskaznik_q;
            *wskaznik_h = *wskaznik_q;   
            wskaznik_q++;
            wskaznik_m++;
            wskaznik_h++;
            wskaznik_pomiar++;
        }

        // ________Q U I C K S O R T__________________________________________________
       
        cout << endl << "Quicksort" << endl;
        
        wskaznik_q = tablica_q;             //PRZED SORTOWANIEM 
        for (int i = 0; i < ile; i++) {
            cout << *wskaznik_q << " ";
            wskaznik_q ++ ;
        }
        
        
        wskaznik_q = tablica_q;         
        start_q = clock();
        Quicksort(wskaznik_q, 0, ile - 1);
        stop_q = clock();
        czas_q = (double)(stop_q - start_q) / CLOCKS_PER_SEC;
        
        
        cout << endl;
        wskaznik_q = tablica_q;
        for (int i = 0; i < ile; i++) {     //PO SORTOWANIU
            cout << *wskaznik_q << " ";
            wskaznik_q ++ ;
        }
        
        //cout << endl << "Czas: " << czas_q << endl;
        
        /*
        plik_q.open("Quick.txt", ios::out | ios::app); //ZAPIS DO PLIKU
        plik_q << czas_q << endl;
        plik_q.close();
        */
        //_____________ M E R G E S O R T_____________________________________________
        
        cout << endl << "Mergesort" << endl;
        
        wskaznik_m = tablica_m;
        wskaznik_m_p = tablica_m_p;         //PRZED SORTOWANIEM 
        for (int i = 0; i < ile; i++) {
            cout << *wskaznik_m << " ";
            wskaznik_m++;
        }
        
        wskaznik_m = tablica_m;
        wskaznik_m_p = tablica_m_p;
        start_m = clock();
        Mergesort(wskaznik_m, wskaznik_m_p, 0, ile - 1);
        stop_m = clock();
        czas_m = (double)(stop_m - start_m) / CLOCKS_PER_SEC;
        
        cout << endl;
        wskaznik_m = tablica_m;
        wskaznik_m_p = tablica_m_p;
        for (int i = 0; i < ile; i++) {        //PO SORTOWANIU
            cout << *wskaznik_m << " ";  
            wskaznik_m++;
        }
        
        //cout << endl << "Czas: " << czas_m << endl;
        /*
        plik_m.open("Merge.txt", ios::out | ios::app);   //ZAPIS DO PLIKU
        plik_m << czas_m << endl;
        plik_m.close();
        */
        // ___________________H E A P S O R T________________________________________________
        
        cout << endl << "Heapsort" << endl;
        
        wskaznik_h = tablica_h;
        wskaznik_h++;                       //PRZED SORTOWANIEM 
        for (int i = 0; i < ile; i++) {
            cout << *wskaznik_h << " ";
            wskaznik_h++;
        }
        
        wskaznik_h = tablica_h;
        start_h = clock();
        Heapsort(wskaznik_h, ile);
        stop_h = clock();
        czas_h = (double)(stop_h - start_h) / CLOCKS_PER_SEC;
        
        wskaznik_h = tablica_h;
        wskaznik_h++;
        cout << endl;                       //PO SORTOWANIU
        for (int i = 0; i < ile; i++) {         
            cout << *wskaznik_h << " ";
            wskaznik_h++;
        }
        
        //cout << endl << "Czas: " << czas_h << endl;
        /*
        plik_h.open("Heap.txt", ios::out | ios::app);       //ZAPIS DO PLIKU
        plik_h << czas_h << endl;
        plik_h.close();
        */
        cout << endl << "===============================================" << endl;

        delete[] tablica_q;
        delete[] tablica_m;
        delete[] tablica_h;
        delete[] tablica_pomiar;
        delete[] tablica_pomiar_p;
       
    }

    return 0;
}

